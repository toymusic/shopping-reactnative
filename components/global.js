module.exports = {
    addProductToCart: null,
    incrQuantity: null,
    decrQuantity: null,
    removeProduct: null,
    refreshCart: null,
    onSignIn: null,
    gotoSearch: null,
    gotoDetail: null,
    setUserId: null,
    setArraySearch: null
};