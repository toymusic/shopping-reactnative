/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, TextInput} from 'react-native';
import icLogo from '../../appIcon/ic_logo.png';
import icBack from '../../appIcon/back_white.png';
import {createUser, getUser} from '../../networking/Server';
import { Base64 } from 'js-base64';
import globalFile from '../global';

export default class Authentication extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isSignIn: true,
            username: '',
            email: '',
            password: '',
            repass: ''
        };
    }
    signIn() {
        this.setState ({
            isSignIn: true,
            username: '', 
            password: '',
        })
    }
    signUp() {
        this.setState ({
            isSignIn: false,
            username: '',
            email: '',
            password: '',
            repass: '' 
        })
    }
    createUserSubmit() {
        let username = this.state.username.trim();
        let email = this.state.email.trim();
        let password = this.state.password;
        let repassword = this.state.repass;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if (username == '') {
            alert('Tên đăng nhập không được để trống');
            return false;
        }
        if (username.length < 8) {
            alert('Tên đăng nhập không được dưới 8 kí tự');
            return false;
        }
        if (email == '') {
            alert('Email không được để trống');
            return false;
        }
        if (reg.test(email) === false) {
            alert('Email không đúng định dạng');
            return false;
        }
        if (password == '') {
            alert('Mật khẩu không được để trống');
            return false;
        }
        if (password.length < 8) {
            alert('Mật khẩu không được dưới 8 kí tự');
            return false;
        }
        if (password != repassword) {
            alert('Xác nhận mật khẩu không trùng khớp');
            return false;
        }
        password = Base64.encode(password);
        createUser(this.state.username, password, email).then((result) => {
            if (result == true) {
                alert('Đăng Kí Thành Công');
            } else {
                alert('Đăng Kí Thất Bại');
            }
        });
    }
    signInSubmit() {
        let email = this.state.email.trim();
        let password = this.state.password;
        if (email == '') {
            alert('Tên đăng nhập không được để trống');
            return false;
        }
        if (password == '') {
            alert('Mật khẩu không được để trống');
            return false;
        }
        
        getUser(email, password).then((result) => {
            if (result == null) {
                alert('Đăng nhập thất bại');
                return false;
            } else {
                this.props.navigation.navigate('Main');
                globalFile.onSignIn(result[0]);
                globalFile.setUserId(result[0].id);
            }
        });
    }
    render() {
        const {container, mainTitleStyle, signUpStyle, signInStyle, controlStyle, header, iconStyle, inputStyle, bigButton, buttonText, disableButton, enableButton} = styles;
        const {isSignIn, username, password, repass, email} = this.state;
        const {navigation} = this.props;

        const signInJSX = (
            <View>
                <TextInput style={inputStyle} placeholder="Enter Your Email" onChangeText={(text) => this.setState({email: text})} value={email}/>
                <TextInput style={inputStyle} placeholder="Enter Your Password" onChangeText={(text) => this.setState({password: text})} value={password} secureTextEntry={true}/>
                <TouchableOpacity style={bigButton} onPress={this.signInSubmit.bind(this)}>
                    <Text style={buttonText}>SIGN IN NOW</Text>
                </TouchableOpacity>
            </View>
        );
        const signUpJSX = (
            <View>
                <TextInput style={inputStyle} placeholder="Enter Your Name" onChangeText={(text) => this.setState({username: text})} value={username}/>
                <TextInput style={inputStyle} placeholder="Enter Your Email" onChangeText={(text) => this.setState({email: text})} value={email}/>
                <TextInput style={inputStyle} placeholder="Enter Your Password" onChangeText={(text) => this.setState({password: text})} value={password} secureTextEntry={true}/>
                <TextInput style={inputStyle} placeholder="Re-Enter Your Password" onChangeText={(text) => this.setState({repass: text})} value={repass} secureTextEntry={true}/>
                <TouchableOpacity style={bigButton} right onPress={this.createUserSubmit.bind(this)}>
                    <Text style={buttonText}>SIGN UP NOW</Text>
                </TouchableOpacity>
            </View>
        );
            
        const mainJSX = isSignIn ? signInJSX : signUpJSX;
        return (
            <View style={container}>
                <View style={header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={icBack} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={mainTitleStyle}>Wearing a Dress</Text>
                    <Image source={icLogo} style={iconStyle}/>  
                </View>
                {mainJSX}
                <View style={controlStyle}>
                    <TouchableOpacity style={signInStyle} onPress={this.signIn.bind(this)}>
                        <Text style={isSignIn ? enableButton  : disableButton }>SIGN IN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={signUpStyle} onPress={this.signUp.bind(this)}>
                        <Text style={isSignIn ? disableButton   : enableButton }>SIGN UP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
module.exports = Authentication;

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#34B089',
        justifyContent: 'space-between',
    },
    header: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    mainTitleStyle: {
        color: '#FFF', 
        fontSize: 20,
        alignItems: 'center'
    },
    controlStyle: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20
    },
    signInStyle: {
        backgroundColor: '#FFF',
        paddingVertical: 10,
        alignItems: 'center',
        flex: 1,
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
        marginRight: 1
    },
    signUpStyle: {
        backgroundColor: '#FFF',
        paddingVertical: 10,
        alignItems: 'center',
        flex: 1,
        borderBottomRightRadius: 20,
        borderTopRightRadius: 20,
        marginLeft: 1
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    inputStyle: {
        backgroundColor: '#FFF',
        paddingVertical: 10,
        borderRadius: 20,
        marginBottom: 10,
        marginLeft: 20,
        marginRight: 20
    },
    bigButton: {
        height: 50,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#FFF',
        marginLeft: 20,
        marginRight: 20
    },
    buttonText: {
        color: '#FFFFFF'
    },
    enableButton: {
        color: '#D7D7D7'
    }, 
    disableButton: {
        color: '#3EBA77'
    } 
});

