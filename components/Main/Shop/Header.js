/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, Image, TouchableOpacity, TextInput} from 'react-native';
import icLogo from '../../../appIcon/ic_logo.png';
import icMenu from '../../../appIcon/ic_menu.png';
import {searchProduct} from '../../../networking/Server';
import globalFile from '../../global';

const {height} = Dimensions.get('window');

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            txtSearch: ''
        };
    }

    onSearch() {
        const { txtSearch } = this.state;
        this.setState({ txtSearch: '' });
        searchProduct(txtSearch)
        .then(arrProduct => {
            globalFile.setArraySearch(arrProduct)
        }
            
            )
        .catch(err => console.log(err));
    }
    render() {
        const {menu, row, textInput, iconStyle, titleStyle } = styles;
        return (
            <View style={menu}>
                <View style={row}>
                    <TouchableOpacity onPress = {this.props.onOpen}>
                        <Image source={icMenu} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={titleStyle}>Wearing A Dress</Text>
                    <Image source={icLogo} style={iconStyle}/>
                </View>
                <TextInput 
                    style={textInput}
                    placeholder="What do you want to buy?"
                    underlineColorAndroid="transparent"
                    value={this.state.txtSearch}
                    onChangeText={text => this.setState({ txtSearch: text })}
                    onFocus={() => globalFile.gotoSearch()} 
                    onSubmitEditing={this.onSearch.bind(this)}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    menu: {
        height: height / 8, 
        backgroundColor: '#34B089',
        padding: 10,
        justifyContent: 'space-around'
    },
    row: {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    textInput: {
        height: height / 18,
        backgroundColor: '#FFF',
        paddingLeft: 10,
        marginTop: 10
    },
    iconStyle: {
        width: 25,
        height: 25
    },
    titleStyle: {
        color: '#FFF',
        fontSize: 20
    }
});

