/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Home from '../Shop/Home/Home';
import Contact from '../Shop/Contact/Contact';
import Cart from '../Shop/Cart/Cart';
import Header from '../Shop/Header';
import homeIcon from '../../../appIcon/home.png';
import contactIcon from '../../../appIcon/contact.png';
import cartIcon from '../../../appIcon/cart.png';
import searchIcon from '../../../appIcon/search.png';
import Search from './Search/Search';
import globalFile from '../../../components/global';
import getCart from '../../../networking/getCart';
import saveCart from '../../../networking/saveCart';


export default class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'home',
            cartList: []
        };
        globalFile.addProductToCart = this.addProductToCart.bind(this);
        globalFile.incrQuantity = this.incrQuantity.bind(this);
        globalFile.decrQuantity = this.decrQuantity.bind(this);
        globalFile.removeProduct = this.removeProduct.bind(this);
        globalFile.refreshCart = this.refreshCart.bind(this);
        globalFile.gotoSearch = this.gotoSearch.bind(this);
        globalFile.gotoDetail = this.gotoDetail.bind(this);
    }
    componentDidMount() {
        getCart().then(cartArray => this.setState({cartList: cartArray}))
    }
    openMenu() {
        const { open } = this.props;
        open();
    }
    addProductToCart(product) {
        const isExist = this.state.cartList.some(e => e.product.id === product.id);
        if (isExist) return false;
        this.setState(
            { cartList: this.state.cartList.concat({ product, quantity: 1 }) }, 
            () => saveCart(this.state.cartList)
        );
    }
    incrQuantity(productId) {
        const newCart = this.state.cartList.map(e => {
            if (e.product.id !== productId) return e;
            return { product: e.product, quantity: e.quantity + 1 };
        });
        this.setState({ cartList: newCart }, 
            () => saveCart(this.state.cartList)
        );
    }

    decrQuantity(productId) {
        const newCart = this.state.cartList.map(e => {
            if (e.product.id !== productId) return e;
            return { product: e.product, quantity: e.quantity - 1 };
        });
        this.setState({ cartList: newCart }, 
            () => saveCart(this.state.cartList)
        );
    }

    removeProduct(productId) {
        const newCart = this.state.cartList.filter(e => e.product.id !== productId);
        this.setState({ cartList: newCart }, 
            () => saveCart(this.state.cartList)
        );
    }
    refreshCart() {
        this.setState({ cartList: [] }, 
            () => saveCart(this.state.cartList)
        );
    }
    gotoSearch() {
        this.setState({ selectedTab: 'search' });
    }
    gotoDetail(product) {
        this.setState({ selectedTab: 'home' });
        this.refs.HomeScreen.changetoDetail(product);
    }
    render() {
        const {iconStyle} = styles;
        const {selectedTab, cartList} = this.state;
        return (
            <View style={{flex: 1, backgroundColor: '#85A6C9'}}>
                <Header onOpen = {this.openMenu.bind(this)}/>
                <TabNavigator>
                    <TabNavigator.Item
                        selected={selectedTab === 'home'}
                        title="Home"
                        renderIcon= {() => <Image source={homeIcon} style={iconStyle} />}
                        onPress={() => {this.setState({ selectedTab: 'home'})}}
                    >
                        <Home ref="HomeScreen"/>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={selectedTab === 'search'}
                        title="Search"
                        renderIcon= {() => <Image source={searchIcon} style={iconStyle} />}
                        onPress={() => {this.setState({ selectedTab: 'search' }), this.refs.HomeScreen.setState({status: 'Home'})}}>
                        <Search/>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={selectedTab === 'contact'}
                        title="Contact"
                        renderIcon= {() => <Image source={contactIcon} style={iconStyle} />}
                        onPress={() => {this.setState({ selectedTab: 'contact' }), this.refs.HomeScreen.setState({status: 'Home'})}}>
                        <Contact/>
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={selectedTab === 'cart'}
                        title="Cart"
                        renderIcon= {() => <Image source={cartIcon} style={iconStyle} />}
                        onPress={() => {this.setState({ selectedTab: 'cart' }) , this.refs.HomeScreen.setState({status: 'Home'})}}
                        badgeText={cartList.length}
                        >
                        <Cart cartList = {cartList}/>
                    </TabNavigator.Item>
                </TabNavigator>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    iconStyle: {
        width: 20,
        height: 20
    }
});

