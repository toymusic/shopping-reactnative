/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, ImageBackground, TouchableOpacity} from 'react-native';
import maxiImage from '../../../../temp/maxi.jpg';
import Swiper from 'react-native-swiper';
import {getCategoriesFromServer } from '../../../../networking/Server';

const {height, width} = Dimensions.get('window');
const url = 'http://192.168.1.53/apishopping/images/type/';

export default class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listCategories: []
        };
        
    }
    componentDidMount() {
        this.refreshDataFromServer();
    }
    refreshDataFromServer = () => {
        getCategoriesFromServer().then((categories) => {
            if (categories.length > 0) {
                this.setState({listCategories: categories});
            } else {
                this.setState({listCategories: []});
            }
            
        }).catch((error) => {
            this.setState({listCategories: []});
        }); 
    }
    render() {
        const {wrapper, imageStyle, categoryContainerStyle} = styles;
        const {listCategories} = this.state;
        const {gotoCategories, gotoLittle} = this.props;
        return (
            <View style={wrapper}>
                <View>
                    <Text>CATEGORY</Text>
                </View>
                <View style={categoryContainerStyle}>
                    <Swiper width={imageWidth} height={imageHeight}>
                        { listCategories.map(e => (
                            <TouchableOpacity onPress={() => gotoCategories(e)}>
                                <ImageBackground source={{ uri: `${url}${e.image}` }} style={imageStyle} />
                            </TouchableOpacity>
                            )) 
                        }
                    </Swiper>
                </View>
            </View>
        );
    }
}
const imageWidth = width - 40;
const imageHeight = imageWidth / 2;

const styles = StyleSheet.create({
    wrapper: {
        width: width - 20,
        height: height / 3.5,
        backgroundColor: '#FFF',
        marginTop: 10,
        marginLeft: 10,
    },
    showImage: {
        flex: 4, 
        paddingLeft: 10, 
        paddingBottom: 50, 
        paddingTop: 20,
    },
    imageStyle: {
        justifyContent: 'center',
        width: imageWidth,
        height: imageHeight
    },
    titleStyle: {
        fontSize: 15,
        fontFamily: 'Avenir',
        color: '#9A9A9A',
        justifyContent: 'center',
        alignItems: 'center'
    },
    categoryContainerStyle: {
        flex: 4, 
        marginLeft: 10, 
        marginBottom: 10
    }
});

