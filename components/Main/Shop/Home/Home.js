/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, ScrollView, Image} from 'react-native';
import Collection from './Collection';
import Category from './Category';
import TopProduct from './TopProduct';
import ProductsByCategory from './ProductsByCategory';
import Detail from './Detail';
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'Home'
        }
    }
    gotoCategories(e) {
        this.setState ({
            status: 'Category',
            name: e.name,
            id: e.id
        })
    }
    changetoDetail(e) {
        this.setState ({
            status: 'Detail',
            product: e
        })
    }
    goHome() {
        this.setState ({
            status: 'Home' 
        })
    }
    render() {
        const {wrapper} = styles;
        const {name, id, status, product} = this.state;
        switch (status) {
            case 'Detail':
                return (
                    <Detail gotoHome = {this.goHome.bind(this)} product = {product} />
                );
            case 'Category':
                return (
                    <ProductsByCategory gotoHome = {this.goHome.bind(this)} title= {name} id = {id} gotoDetail = {this.changetoDetail.bind(this)}/>
                );
            default:
                return (
                    <ScrollView style={wrapper}>
                        <Collection/>
                        <Category gotoCategories = {this.gotoCategories.bind(this)} />
                        <TopProduct gotoDetail = {this.changetoDetail.bind(this)}/>
                    </ScrollView>
                );
        }
    }
}
module.exports = Home;
const styles = StyleSheet.create({
    wrapper: {
        flex:1, 
        backgroundColor: '#9A9A9A'
    }
})
