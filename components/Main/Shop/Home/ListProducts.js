/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, ImageBackground, TouchableOpacity} from 'react-native';

const {height, width} = Dimensions.get('window');

export default class ListProducts extends Component {
    render() {
        const {wrapper} = styles;
        return (
            <View style={wrapper}>
                <Text>List products</Text>
            </View>
        );
    }
}
module.exports = ListProducts;

const styles = StyleSheet.create({
    wrapper: {
        flex:1, 
        backgroundColor: '#9A9A9A'
    }
});
