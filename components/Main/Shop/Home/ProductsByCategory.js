/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, Image, ScrollView, TouchableOpacity, ListView} from 'react-native';
import icBack from '../../../../appIcon/back.png';
import {getProductsByCategoryFromServer} from '../../../../networking/Server';
const url = 'http://192.168.1.53/apishopping/images/products/';

const {height, width} = Dimensions.get('window');

export default class MaxiProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listProducts: []
        };
    }
    componentDidMount() {
        this.refreshDataFromServer();
    }
    refreshDataFromServer = () => {
        getProductsByCategoryFromServer(this.props.id).then((products) => {
            this.setState({listProducts: products});
        }).catch((error) => {
            this.setState({listProducts: []});
        }); 
    }
    render() {
        const {wrapper, container, iconStyle, header, mainTitleStyle, content, productInfo, txtName, txtPrice, txtMaterial, lastRowInfo, txtColor, productImage, txtShowDetail, main } = styles;
        const {title, gotoDetail} = this.props;
        const {listProducts} = this.state;
        return (
            <View style={container}>
                <ScrollView style={wrapper}>
                    <View style={header}>
                        <TouchableOpacity onPress={this.props.gotoHome}>
                            <Image source={icBack} style={iconStyle} />
                        </TouchableOpacity>
                        <Text style={mainTitleStyle}>{title}</Text>
                        <View style={{width: 30}} /> 
                    </View>
                    <ListView
                        contentContainerStyle={main}
                        enableEmptySections
                        dataSource={new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }).cloneWithRows(listProducts)}
                        renderRow={product => (
                            <View style={content}>
                                <Image source={{ uri: `${url}${product.image}` }} style={productImage}/>
                                <View style={productInfo}>
                                    <Text style={txtName}>{product.name}</Text>
                                    <Text style={txtPrice}>{product.price} $</Text>
                                    <Text style={txtMaterial}>Material Silk: {product.material}</Text>
                                    <View style= {lastRowInfo}>
                                        <Text style={txtColor}>{product.color}</Text>
                                        <View style={{ backgroundColor: product.color, height: 16, width: 16, borderRadius: 8}} />
                                        <TouchableOpacity onPress={() => gotoDetail(product)}>
                                            <Text style={txtShowDetail}>Show Detail</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#9A9A9A',
    },
    header: {
        height: 50,
        flexDirection: 'row', 
        backgroundColor: '#FFF',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    main: {
        backgroundColor: '#FFF'
    },
    mainTitleStyle: {
        fontSize: 20,
        color: '#B10D65',
    },
    iconStyle: {
        width: 25,
        height: 25,
        color: '#B10D65',
    },
    wrapper: {
        backgroundColor: '#FFF',
        margin: 10,
        paddingHorizontal: 10,
    },
    content: {
        flexDirection: 'row',
        paddingVertical: 15,
        borderTopColor: '#F0F0F0',
        borderBottomColor: '#FFF',
        borderLeftColor: '#FFF',
        borderRightColor: '#FFF',
        borderWidth: 1
    },
    productInfo: {
        justifyContent: 'space-between',
        marginLeft: 15,
        flex: 1
    },
    productImage: {
        width: 90,
        height: (90 * 452) / 361
    },
    lastRowInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    txtShowDetail: {
        fontFamily: 'Avenir',
        color: '#B10D65',
        fontSize: 11
    },
    txtPrice: {
        color: '#B10D65',
    }
});
