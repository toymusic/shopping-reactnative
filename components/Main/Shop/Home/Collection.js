/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import bannerImage from '../../../../temp/banner.jpg';


const {width, height} = Dimensions.get('window');

export default class Collection extends Component {
    render() {
        const {wrapper, imageStyle} = styles;
        return (
            <View style={wrapper}>
                <View>
                    <Text>SPRING COLLECTION</Text>
                </View>
                <View style={{flex: 4, paddingLeft: 10, paddingBottom: 50}}>
                    <Image source={bannerImage} style={imageStyle} />
                </View>
                
            </View>
        );
    }
}
const imageWidth = width - 40;
const imageHeight = (imageWidth / 1200) * 460;

const styles = StyleSheet.create({
    wrapper: {
        width: width - 20,
        height: height / 3.5,
        backgroundColor: '#FFF',
        marginLeft: 10,
        marginTop: 10
    },
    imageStyle: {
        width: imageWidth,
        height: imageHeight,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    }
})

