/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Dimensions, Image, TouchableOpacity, ListView} from 'react-native';
import {getTopProductsFromServer} from '../../../../networking/Server';

const {width, height} = Dimensions.get('window');
const productWidth = (width - 60) / 2;
const productImageHeight = (productWidth / 361) * 452;
const url = 'http://192.168.1.53/apishopping/images/products/';

export default class TopProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listProducts: []
        };  
    }
    componentDidMount() {
        this.refreshDataFromServer();
    }
    refreshDataFromServer = () => {
        getTopProductsFromServer().then((products) => {
            this.setState({listProducts: products});
        }).catch((error) => {
            this.setState({listProducts: []});
        }); 
    }
    render() {
        const {container, titleContainer, title, body, productContainer, productImage, productName, productPrice, main} = styles;
        const {listProducts} = this.state;
        const {gotoDetail} = this.props;
        return (
            <View style={container}>
                <View style={titleContainer}>
                    <Text style={title}>TOP PRODUCT</Text>
                </View>
                <ListView
                        contentContainerStyle={body}
                        enableEmptySections
                        dataSource={new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }).cloneWithRows(listProducts)}
                        renderRow={product => (
                            <TouchableOpacity style={productContainer} onPress={() => gotoDetail(product)}>
                                <Image source={{ uri: `${url}${product.image}` }} style={productImage}/>
                                <Text style={productName}>{product.name}</Text>
                                <Text style={productPrice}>{product.price} $</Text>
                            </TouchableOpacity>
                        )}
                    />
            </View>
        );
    }
}
module.exports = TopProduct;
const imageWidth = (width - 50) / 2;
const imageHeight = (imageWidth / 361) * 452;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        margin: 10
    },
    titleContainer: {
        height: 50,
        justifyContent: 'center',
        paddingLeft: 10
    },
    title: {
        color: '#D3D3CF',
        fontSize: 20
    },
    body: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
    },
    productContainer: {
        width: imageWidth,
        marginLeft: 6,
        marginRight: 6
    },
    productImage: {
        width: imageWidth,
        height: imageHeight
    },
    productName: {
        paddingLeft: 10,
        color: '#D3D3CF',
        fontWeight: '500'
    },
    productPrice: {
        paddingLeft: 10,
        color: '#662F90',
        marginBottom: 5
    },
    main: {
        backgroundColor: '#FFF'
    },
})

