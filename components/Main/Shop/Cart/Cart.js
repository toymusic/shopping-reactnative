/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, Dimensions, ScrollView, Image, ListView, TouchableOpacity} from 'react-native';
import back from '../../../../appIcon/back.png';
import cart from '../../../../appIcon/cart.png';
const url = 'http://192.168.1.53/apishopping/images/products/';
import globalFile from '../../../global';
import getUserLogIn from '../../../../networking/getUserLogIn';
import {sendOrder} from '../../../../networking/Server';

function toTitleCase(str) {
    return str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}

export default class Cart extends Component {
    constructor(props) {
        super(props);
        globalFile.setUserId = this.setUserId.bind(this);
    }
    componentDidMount() {
        getUserLogIn().then(user_info => this.setState({user: user_info}));
    }
    incrQuantity(id) {
        globalFile.incrQuantity(id);
    }
    decrQuantity(id) {
        globalFile.decrQuantity(id);
    }
    removeProduct(id) {
        globalFile.removeProduct(id);
    }
    setUserId(id) {
        this.setState({
            user: {
                id: id
            }
        })
    }
    async onSendOrder(total) {
        let user_id = this.state.user.id;
        if (user_id == null) {
            alert('Đăng nhập tài khoản trước đã');
        } else {
            try {
                const arrayDetail = this.props.cartList.map(e => ({ 
                    id: parseInt(e.product.id), 
                    quantity: e.quantity, 
                    price: parseInt(e.product.price)
                }));
                sendOrder(user_id, total, arrayDetail).then((result) => {
                    if (result == true) {
                        alert('Đặt hàng thành công');
                        globalFile.refreshCart();
                    } else {
                        alert('Đặt hàng thất bại');
                    }
                    
                });
            } catch (e) {
                console.log(e);
            }
        }
        
    }
    render() {
        const {cartList} = this.props;
        const { main, checkoutButton, checkoutTitle, wrapper,
            productStyle, mainRight, productController,
            txtName, txtPrice, productImage, numberOfProduct,
            txtShowDetail, showDetailContainer, header, backStyle } = styles;
        const arrTotal = cartList.map(e => e.product.price * e.quantity);
        const total = arrTotal.length ? arrTotal.reduce((a, b) => a + b) : 0;
        return (
            <View style={wrapper}>
                <ListView
                    contentContainerStyle={main}
                    enableEmptySections
                    dataSource={new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }).cloneWithRows(cartList)}
                    renderRow={cartItem => (
                        <View style={productStyle}>
                            <Image source={{ uri: `${url}${cartItem.product.image}` }} style={productImage} />
                            <View style={[mainRight]}>
                                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                    <Text style={txtName}>{toTitleCase(cartItem.product.name)}</Text>
                                    <TouchableOpacity onPress={() => this.removeProduct(cartItem.product.id)}>
                                        <Text style={{ fontFamily: 'Avenir', color: '#969696' }}>X</Text>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Text style={txtPrice}>{cartItem.product.price}$</Text>
                                </View>
                                <View style={productController}>
                                    <View style={numberOfProduct}>
                                        <TouchableOpacity onPress={() => this.incrQuantity(cartItem.product.id)}>
                                            <Text>+</Text>
                                        </TouchableOpacity>
                                        <Text>{cartItem.quantity}</Text>
                                        <TouchableOpacity onPress={() => this.decrQuantity(cartItem.product.id)}>
                                            <Text>-</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    )}
                />
                <TouchableOpacity style={checkoutButton} onPress={() => { this.onSendOrder(total)}} >

                    <Text style={checkoutTitle}>TOTAL {total}$ CHECKOUT NOW</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const { width } = Dimensions.get('window');
const imageWidth = width / 4;
const imageHeight = (imageWidth * 452) / 361;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: '#DFDFDF'
    },
    checkoutButton: {
        height: 50,
        margin: 10,
        marginTop: 0,
        backgroundColor: '#2ABB9C',
        borderRadius: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    main: {
        width, backgroundColor: '#DFDFDF'
    },
    checkoutTitle: {
        color: '#FFF',
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'Avenir'
    },
    productStyle: {
        flexDirection: 'row',
        margin: 10,
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 2,
        shadowColor: '#3B5458',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2
    },
    productImage: {
        width: imageWidth,
        height: imageHeight,
        flex: 1,
        resizeMode: 'center'
    },
    mainRight: {
        flex: 3,
        justifyContent: 'space-between'
    },
    productController: {
        flexDirection: 'row'
    },
    numberOfProduct: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    txtName: {
        paddingLeft: 20,
        color: '#A7A7A7',
        fontSize: 20,
        fontWeight: '400',
        fontFamily: 'Avenir'
    },
    txtPrice: {
        paddingLeft: 20,
        color: '#C21C70',
        fontSize: 20,
        fontWeight: '400',
        fontFamily: 'Avenir'
    },
    txtShowDetail: {
        color: '#C21C70',
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Avenir',
        textAlign: 'right',
    },
    showDetailContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1,
        paddingHorizontal: 15,
        paddingTop: 20
    },
    backStyle: {
        width: 25,
        height: 25
    },
});
