/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import profileIcon from '../../temp/profile.png'; 
import globalFile from '../global';
import getUserLogIn from '../../networking/getUserLogIn';
import saveUserLogin from '../../networking/saveUserLogin';
import {changeInfoUser} from '../../networking/Server';

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        };
        globalFile.onSignIn = this.saveUserInfo.bind(this);
        globalFile.updateUser = this.submitInfo.bind(this);
    }
    componentDidMount() {
        getUserLogIn().then(user_info => this.setState({user: user_info}));
    }
    saveUserInfo(user_info) {
        this.setState(
            {user: user_info},
            () => saveUserLogin(this.state.user)
        ),
        globalFile.setUserId(user_info.id)
    }
    submitInfo(id, txtName, txtAddress, txtPhone) {
        changeInfoUser(id, txtName, txtAddress, txtPhone).then((result) => {
            if (result == false) {
                alert('Cập nhật thông tin thất bại')
                return false;
            } else {
                this.setState({
                    user: {
                        id: id,
                        full_name: txtName,
                        address: txtAddress,
                        phone: txtPhone
                    }
                });
                saveUserLogin(this.state.user);
                alert('Cập nhật thông tin thành công');
            }
        });
    }
    render() {
        const {container, profileStyle, btnText, btnStyle, profileContainerStyle, titleName} = styles;
        const {user} = this.state;
        const logoutJSK = (
            <View style={{flex: 1}}>
                <TouchableOpacity style={btnStyle} onPress={() => this.props.authentication()}>
                    <Text style={btnText}>Sign In</Text>
                </TouchableOpacity>
            </View>
        );
        const loginJSK = (
            <View style={profileContainerStyle}>
                <Text style={titleName}>{user.full_name}</Text>
                <View>
                    <TouchableOpacity style={btnStyle} onPress={() => this.props.orderHistory()}>
                        <Text style={btnText}>Order History</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={btnStyle} onPress={() => this.props.changeInfo()}>
                        <Text style={btnText}>Change Info</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={btnStyle} onPress={() => this.saveUserInfo({})} >
                        <Text style={btnText}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
        
        const mainJSX = isEmpty(user) ? logoutJSK  : loginJSK;
        return (
            <View style={container}>
                <Image source ={profileIcon} style={profileStyle} />
                {mainJSX}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: '#34B089',
        alignItems: 'center',
    },
    profileStyle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 30
    },
    btnStyle: {
        height: 50,
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        width: 200,
        marginBottom: 10,
    },
    btnText: {
        color: '#34B089',
        fontSize: 15
    },
    profileContainerStyle: {
        flex: 1, 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    titleName: {
        color: '#FFF', 
        fontSize: 20
    }
});


