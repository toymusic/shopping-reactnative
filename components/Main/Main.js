/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import Menu from './Menu';
import Shop from './Shop/Shop';
import Drawer from 'react-native-drawer';

export default class Main extends Component {
    closeControlPanel = () => {
        this.drawer.close()
    }
    openControlPanel = () => {
        this.drawer.open()
    }
    gotoAuthentication = () => {
        this.props.navigation.navigate('Authentication')
    }
    gotoOrderHistory = () => {
        this.props.navigation.navigate('OrderHistory')
    }
    gotoChangeInfo = () => {
        this.props.navigation.navigate('ChangeInfo')
    }
    render() {
        const { navigation } = this.props;
        return (
            <Drawer 
                ref = {(ref) => {this.drawer = ref;}} 
                content = {<Menu authentication = {this.gotoAuthentication.bind(this)} 
                orderHistory = {this.gotoOrderHistory.bind(this)}
                changeInfo = {this.gotoChangeInfo.bind(this)}
                />} 
                openDrawerOffset={0.4}
                
            >
                <Shop open = {this.openControlPanel.bind(this)}/>
            </Drawer>
        );
    }
}
module.exports = Main;


