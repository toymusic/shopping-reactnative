import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

var MainScreen = require('./components/Main/Main');
var AuthenticationScreen = require('./components/Authentication/Authentication');
var OrderHistoryScreen = require('./components/OrderHistory/OrderHistory');
var ChangeInfoScreen = require('./components/ChangeInfo/ChangeInfo');

const RootStack = createStackNavigator(
  {
      Main: MainScreen,
      Authentication: AuthenticationScreen,
      OrderHistory: OrderHistoryScreen,
      ChangeInfo: ChangeInfoScreen,
  },
  {
      initialRouteName: 'Main',
      headerMode: 'none'
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
