const apiGetAllCategories = 'http://192.168.1.53/apishopping/getListCategories.php';
const apiGetTopProducts = 'http://192.168.1.53/apishopping/getListTopProducts.php';
const apiGetProductsByCategory = 'http://192.168.1.53/apishopping/getListProductsByCategory.php';
const apiCreateUser = 'http://192.168.1.53/apishopping/createUser.php';
const apiChangeInfoUser = 'http://192.168.1.53/apishopping/changeInfoUser.php';
const apiSignIn = 'http://192.168.1.53/apishopping/getUser.php';
const apiSendOrder = 'http://192.168.1.53/apishopping/sendOrder.php';
const apiGetOrderHistory = 'http://192.168.1.53/apishopping/getOrderHistory.php';
const apiSearchProduct = 'http://192.168.1.53/apishopping/searchProduct.php';


async function getCategoriesFromServer() {
    try {
        const response = await fetch(apiGetAllCategories);
        let responseJson = await response.json();
        return responseJson.categories;
    } catch (error) {
        console.log(error);
    }
}
async function getTopProductsFromServer() {
    try {
        const response = await fetch(apiGetTopProducts);
        let responseJson = await response.json();
        return responseJson.products;
    } catch (error) {
        console.log(error);
    }
}
async function getProductsByCategoryFromServer(params) {
    try {
        const formData = new FormData();
        formData.append("category_id", params);
        const response = await fetch(apiGetProductsByCategory, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.products;
    } catch (error) {
        console.log(error);
    }
}
async function createUser(name, password, email) {
    try {
        const formData = new FormData();
        formData.append("name", name);
        formData.append("password", password);
        formData.append("email", email);
        const response = await fetch(apiCreateUser, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.status;
    } catch (error) {
        console.log(error);
    }
}
async function changeInfoUser(id, full_name, address, phone) {
    try {
        const formData = new FormData();
        formData.append("id", id);
        formData.append("full_name", full_name);
        formData.append("address", address);
        formData.append("phone", phone);
        const response = await fetch(apiChangeInfoUser, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.status;
    } catch (error) {
        console.log(error);
    }
}
async function getUser(email, password) {
    try {
        const formData = new FormData();
        formData.append("email", email);
        formData.append("password", password);
        const response = await fetch(apiSignIn, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.user;
    } catch (error) {
        console.log(error);
    }
}
async function sendOrder(user_id, total, arrayDetails) {
    try {
        const formData = new FormData();
        formData.append("user_id", user_id);
        formData.append("total", total);
        formData.append("list_products_order", JSON.stringify(arrayDetails));
        const response = await fetch(apiSendOrder, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.status;
    } catch (error) {
        console.log(error);
    }
}
async function getOrderHistory(user_id) {
    try {
        const formData = new FormData();
        formData.append("user_id", user_id);
        const response = await fetch(apiGetOrderHistory, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.orders;
    } catch (error) {
        console.log(error);
    }
}
async function searchProduct(name) {
    try {
        const formData = new FormData();
        formData.append("name", name);
        const response = await fetch(apiSearchProduct, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body: formData,
        });
        let responseJson = await response.json();
        return responseJson.products;
    } catch (error) {
        console.log(error);
    }
}
export {getCategoriesFromServer, getTopProductsFromServer, getProductsByCategoryFromServer, createUser, getUser, sendOrder, getOrderHistory, changeInfoUser, searchProduct};

