import AsyncStorage from '@react-native-community/async-storage';

const saveUserLogin = async (user) => {
    await AsyncStorage.setItem('@user', JSON.stringify(user));
};

export default saveUserLogin;