import AsyncStorage from '@react-native-community/async-storage';

const getUserLogIn = async () => {
    try {
        const value = await AsyncStorage.getItem('@user');
        if (value !== null) {
            return JSON.parse(value);
        }
        return [];
    } catch (error) {
    // Error retrieving data
        return [];
    }
};

export default getUserLogIn;